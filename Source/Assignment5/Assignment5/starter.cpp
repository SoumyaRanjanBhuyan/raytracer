#include <stdio.h>
#include <stdlib.h>

#include <windows.h>
#include "GL/gl.h"
#include "GL/glu.h"
#include "GL/glut.h"


// The Width & Height have been reduced to help you debug
//	more quickly by reducing the resolution 
//  Your code should work for any dimension, and should be set back
//	to 640x480 for submission.

//#define WINDOW_WIDTH 320
//#define WINDOW_HEIGHT 240
#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480

#include "Scene.h"
#include "RayTrace.h"
#include "NormalRenderer.h"

/* --- Global State Variables --- */
float w = 0.0f, h = 0.0f;
Vector nearCenter(0.0f,0.0f,0.0f);

Vector currentPix(0.0f,0.0f,0.0f);
Vector firstPixel(0.0f,0.0f,0.0f);

Vector R(0.0f,0.0f,0.0f);
Vector up(0.0f,0.0f,0.0f);

Vector L;
float D;
float aspectRatio;

//Menu options
const int SCENE_SAMPLE = 9;
const int SCENE_1 = 10;
const int SCENE_2 = 11;
const int SCENE_3 = 12;

// controling parameters
int mouse_button;
int mouse_x		= 0;
int mouse_y		= 0;
float scale		= 1.0;
float x_angle	= 0.0;
float y_angle	= 0.0;

/* - Menu State Identifier - */
int g_iMenuId;

/* - Mouse State Variables - */
int g_vMousePos[2] = {0, 0};
int g_iLeftMouseButton = 0;    /* 1 if pressed, 0 if not */
int g_iMiddleMouseButton = 0;
int g_iRightMouseButton = 0;

/* - RayTrace Variable for the RayTrace Image Generation - */
RayTrace g_RayTrace_Sample,g_RayTrace1,g_RayTrace2, g_RayTrace3;

/* - NormalRenderer Variable for drawing with OpenGL calls instead of the RayTracer - */
NormalRenderer g_NormalRenderer;

/* - RayTrace Buffer - */
Vector g_ScreenBuffer[WINDOW_HEIGHT][WINDOW_WIDTH];

unsigned int g_X = 0, g_Y = 0;
bool g_bRayTrace = false;
bool g_bRenderNormal = true;
bool scene_1=false,scene_2=false,scene_3=false,scene_sample=false;

void myinit()
{
	// Default to these camera settings
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity ();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Set the Scene Variable for the NormalRenderer
	g_NormalRenderer.SetScene (&g_RayTrace_Sample.m_Scene);

	aspectRatio = (float)WINDOW_WIDTH/(float)WINDOW_HEIGHT;

	L = g_RayTrace_Sample.m_Scene.GetCamera().target - g_RayTrace_Sample.m_Scene.GetCamera().position;
	L.Normalize();
	D = (L * g_RayTrace_Sample.m_Scene.GetCamera().nearClip).Magnitude();
	h = 2 * D * tanf((float)(g_RayTrace_Sample.m_Scene.GetCamera().fieldOfView)/2.0);
	
	w = h * aspectRatio;

	nearCenter = g_RayTrace_Sample.m_Scene.GetCamera().position + L * D;
	R = L.Cross(g_RayTrace_Sample.m_Scene.GetCamera().up);
	up = g_RayTrace_Sample.m_Scene.GetCamera().up;

	glClearColor(0, 0, 0, 0);

	//currentPix = nearCenter + R * (w * ((float)g_X/WINDOW_WIDTH) - (float)w/2.0) + up * (h * ((float)g_Y/WINDOW_HEIGHT) - (float)h/2.0);

	scene_sample = true;
}

void display()
{
	if (g_bRenderNormal)
	{
		g_NormalRenderer.RenderScene ();
	}
	else
	{
		// Set up the camera to render pixel-by-pixel
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity ();
		glOrtho(0, WINDOW_WIDTH, 0, WINDOW_HEIGHT, 1, -1);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glBegin(GL_POINTS);
		{
			for (int y = 0; y < WINDOW_HEIGHT; y++)
			{
				for (int x = 0; x < WINDOW_WIDTH; x++)
				{
					glColor3f(g_ScreenBuffer[y][x].x, g_ScreenBuffer[y][x].y, g_ScreenBuffer[y][x].z);
					glVertex2i(x, y);
				}
			}
		}

		glEnd();

	}

	glFlush();

	glutSwapBuffers ();
}

void menufunc(int value)
{
	switch (value)
	{
	case 9:
		// Set the Scene Variable for the NormalRenderer
		g_NormalRenderer.SetScene (&g_RayTrace_Sample.m_Scene);
		aspectRatio = (float)WINDOW_WIDTH/(float)WINDOW_HEIGHT;

		L = g_RayTrace_Sample.m_Scene.GetCamera().target - g_RayTrace_Sample.m_Scene.GetCamera().position;
		L.Normalize();
		D = (L * g_RayTrace_Sample.m_Scene.GetCamera().nearClip).Magnitude();
		h = 2 * D * tanf((float)(g_RayTrace_Sample.m_Scene.GetCamera().fieldOfView)/2.0);
	
		w = h * aspectRatio;

		nearCenter = g_RayTrace_Sample.m_Scene.GetCamera().position + L * D;
		R = L.Cross(g_RayTrace_Sample.m_Scene.GetCamera().up);
		up = g_RayTrace_Sample.m_Scene.GetCamera().up;

		glClearColor(0, 0, 0, 0);

		scene_sample = true;
		scene_1 = false;
		scene_2 = false;
		scene_3 = false;
		g_X = 0;
		g_Y = 0;
		glutPostRedisplay ();
		break;

	case 10:
		// Set the Scene Variable for the NormalRenderer
		g_NormalRenderer.SetScene (&g_RayTrace1.m_Scene);
		aspectRatio = (float)WINDOW_WIDTH/(float)WINDOW_HEIGHT;

		L = g_RayTrace1.m_Scene.GetCamera().target - g_RayTrace1.m_Scene.GetCamera().position;
		L.Normalize();
		D = (L * g_RayTrace1.m_Scene.GetCamera().nearClip).Magnitude();
		h = 2 * D * tanf((float)(g_RayTrace1.m_Scene.GetCamera().fieldOfView)/2.0);
	
		w = h * aspectRatio;

		nearCenter = g_RayTrace1.m_Scene.GetCamera().position + L * D;
		R = L.Cross(g_RayTrace1.m_Scene.GetCamera().up);
		up = g_RayTrace1.m_Scene.GetCamera().up;

		glClearColor(0, 0, 0, 0);

		scene_sample = false;
		scene_1 = true;
		scene_2 = false;
		scene_3 = false;
		g_X = 0;
		g_Y = 0;
		glutPostRedisplay ();
		break;
	case 11:
		// Set the Scene Variable for the NormalRenderer
		g_NormalRenderer.SetScene (&g_RayTrace2.m_Scene);
		aspectRatio = (float)WINDOW_WIDTH/(float)WINDOW_HEIGHT;

		L = g_RayTrace2.m_Scene.GetCamera().target - g_RayTrace2.m_Scene.GetCamera().position;
		L.Normalize();
		D = (L * g_RayTrace2.m_Scene.GetCamera().nearClip).Magnitude();
		h = 2 * D * tanf((float)(g_RayTrace2.m_Scene.GetCamera().fieldOfView)/2.0);
	
		w = h * aspectRatio;

		nearCenter = g_RayTrace2.m_Scene.GetCamera().position + L * D;
		R = L.Cross(g_RayTrace2.m_Scene.GetCamera().up);
		up = g_RayTrace2.m_Scene.GetCamera().up;

		glClearColor(0, 0, 0, 0);

		scene_sample = false;
		scene_1 = false;
		scene_2 = true;
		scene_3 = false;
		g_X = 0;
		g_Y = 0;
		glutPostRedisplay ();
		break;

	case 12:
		// Set the Scene Variable for the NormalRenderer
		g_NormalRenderer.SetScene (&g_RayTrace3.m_Scene);
		aspectRatio = (float)WINDOW_WIDTH/(float)WINDOW_HEIGHT;

		L = g_RayTrace3.m_Scene.GetCamera().target - g_RayTrace3.m_Scene.GetCamera().position;
		L.Normalize();
		D = (L * g_RayTrace3.m_Scene.GetCamera().nearClip).Magnitude();
		h = 2 * D * tanf((float)(g_RayTrace3.m_Scene.GetCamera().fieldOfView)/2.0);
	
		w = h * aspectRatio;

		nearCenter = g_RayTrace3.m_Scene.GetCamera().position + L * D;
		R = L.Cross(g_RayTrace3.m_Scene.GetCamera().up);
		up = g_RayTrace3.m_Scene.GetCamera().up;

		glClearColor(0, 0, 0, 0);

		scene_sample = false;
		scene_1 = false;
		scene_2 = false;
		scene_3 = true;
		g_X = 0;
		g_Y = 0;
		glutPostRedisplay ();
		break;
	case 0:
		// Start the Ray Tracing
		g_bRayTrace = true;
		g_bRenderNormal = false;
		glutPostRedisplay ();
		break;
	case 1:
		// Render Normal
		g_bRayTrace = false;
		g_X = 0;
		g_Y = 0;
		g_bRenderNormal = true;
		glutPostRedisplay ();
		break;
	case 2:
		// Quit Program
		exit(0);
		break;
	}
}

void doIdle()
{
	if (g_bRayTrace)
	{		
		currentPix = nearCenter + R * (w * ((float)g_X/WINDOW_WIDTH) - (float)w/2.0) + up * (h * ((float)g_Y/WINDOW_HEIGHT) - (float)h/2.0);

		if(scene_sample)
			g_ScreenBuffer[g_Y][g_X] = g_RayTrace_Sample.generatePicturePerspective(currentPix.x, currentPix.y,currentPix.z);
		else if(scene_1)
			g_ScreenBuffer[g_Y][g_X] = g_RayTrace1.generatePicturePerspective(currentPix.x, currentPix.y,currentPix.z);
		else if(scene_2)
			g_ScreenBuffer[g_Y][g_X] = g_RayTrace2.generatePicturePerspective(currentPix.x, currentPix.y,currentPix.z);
		else if(scene_3)
			g_ScreenBuffer[g_Y][g_X] = g_RayTrace3.generatePicturePerspective(currentPix.x, currentPix.y,currentPix.z);

		// Move to the next pixel
		g_X++;
		if (g_X >= WINDOW_WIDTH)
		{
			// Move to the next row
			g_X = 0;
			g_Y++;

			//You can uncomment the next line to see the raytrace update each step
			glutPostRedisplay();
		}

		// Check for the end of the screen
		if (g_Y >= WINDOW_HEIGHT)
		{
			g_bRayTrace = false;
			glutPostRedisplay ();
		}
	}
	else
	{
		//printf("mycount2 = %d\n",mycount2);
		glutPostRedisplay ();
	}
}

void mousebutton(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		g_iLeftMouseButton = (state==GLUT_DOWN);
		break;
	case GLUT_MIDDLE_BUTTON:
		g_iMiddleMouseButton = (state==GLUT_DOWN);
		break;
	case GLUT_RIGHT_BUTTON:
		g_iRightMouseButton = (state==GLUT_DOWN);
		break;
	}

	g_vMousePos[0] = x;
	g_vMousePos[1] = y;
}


void motion(int x, int y)
{

	if(mouse_button == GLUT_LEFT_BUTTON)
	{
		// compute the angle (0..360) around x axis, and y-axis
		y_angle += (float(x - mouse_x)/WINDOW_WIDTH)*360.0;
		x_angle += (float(y - mouse_y)/WINDOW_HEIGHT)*360.0;

	}
/*
	if(mouse_button == GLUT_RIGHT_BUTTON)
	{
		// scale
		scale += (y-mouse_y)/100.0;
			
		if(scale < 0.1) scale = 0.1;     // too small
		if(scale > 7)	scale = 7;		 // too big

	}
*/
	mouse_x	= x;		// update current mouse position
	mouse_y	= y;
	glutPostRedisplay();

}


int main (int argc, char ** argv)
{
	//You will be creating a menu to load in scenes
	//The test.xml is the default scene and you will modify this code
	if (!g_RayTrace_Sample.m_Scene.Load ("Sample.xml"))
	{
		printf ("Failed to load scene from Sample.xml\n");
		getchar();
		exit(1);
	}
	if (!g_RayTrace1.m_Scene.Load ("Scene1.xml"))
	{
		printf ("Failed to load scene from Scene1.xml\n");
		getchar();
		exit(1);
	}
	if (!g_RayTrace2.m_Scene.Load ("Scene2.xml"))
	{
		printf ("Failed to load scene from Scene2.xml\n");
		getchar();
		exit(1);
	}
	if (!g_RayTrace3.m_Scene.Load ("Scene3.xml"))
	{
		printf ("Failed to load scene from Scene3.xml\n");
		getchar();
		exit(1);
	}

	glutInit(&argc,argv);

	/* create a window */
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);

	glutCreateWindow("Assignment 5 - Ray Tracer");

	/* tells glut to use a particular display function to redraw */
	glutDisplayFunc(display);

	/* create a right mouse button menu */
	GLint subMenu_Scene = glutCreateMenu(menufunc);

	glutAddMenuEntry("Sample Scene", SCENE_SAMPLE);
	glutAddMenuEntry("Scene 1 - Reflective Cylinder", SCENE_1);
	glutAddMenuEntry("Scene 2 - Reflection", SCENE_2);
	glutAddMenuEntry("Scene 3 - Reflection + Refraction", SCENE_3);

	g_iMenuId = glutCreateMenu(menufunc);

	glutSetMenu(g_iMenuId);
	glutAddSubMenu("Scene", subMenu_Scene);
	glutAddMenuEntry("Render RayTrace",0);
	glutAddMenuEntry("Render Normal",1);
	glutAddMenuEntry("Quit",2);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

	/* callback for mouse button changes */
	glutMouseFunc(mousebutton);
	glutMotionFunc(motion);

	/* callback for idle function */
	glutIdleFunc(doIdle);

	/* do initialization */
	myinit();

	glutMainLoop();
	return 0;
}
