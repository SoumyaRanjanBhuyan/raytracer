/*
  NOTE: This is the file you will need to begin working with
		You will need to implement the RayTrace::CalculatePixel () function

  This file defines the following:
	RayTrace Class
*/

#ifndef RAYTRACE_H
#define RAYTRACE_H

#include <stdio.h>
#include <stdlib.h>
//#include <vector.h>

#include "Utils.h"

//#define min(a,b)            (((a) < (b)) ? (a) : (b))

/*
	RayTrace Class - The class containing the function you will need to implement

	This is the class with the function you need to implement
*/

struct ray {
	Vector start;
	Vector dir;
};

typedef struct modelTrTracker{
	int index;
	float tValue;
}modelTrTracker;

vector<ray> reflectionRayList;
ray reflectionRay;
ray refractionRay;

class RayTrace
{
public:
	/* - Scene Variable for the Scene Definition - */
	Scene m_Scene;

	// -- Constructors & Destructors --
	RayTrace (void) {}
	~RayTrace (void) {}

	// -- Main Functions --

//*********************************************/
//			Ray-Sphere Intersection
//*********************************************/
	bool sphereIntersection(ray &r,SceneSphere &s,float &t){

		float A,B,C,D;
		float t0=0.0f,t1=0.0f;

		A = r.dir.x*r.dir.x + r.dir.y*r.dir.y + r.dir.z*r.dir.z;
		B =  2*(r.dir.x*(r.start.x - s.center.x) + r.dir.y*(r.start.y - s.center.y) + 
			 r.dir.z*(r.start.z - s.center.z));
		C =   (r.start.x - s.center.x)*(r.start.x - s.center.x)
			+ (r.start.y - s.center.y)*(r.start.y - s.center.y)
			+ (r.start.z - s.center.z)*(r.start.z - s.center.z) - (s.radius * s.radius);

		D = B*B - 4*A*C;

		if(D < 0.0f)
			return false;

		t0 = ((-1)*B + sqrtf(D))/2*A;
		t1 = ((-1)*B - sqrtf(D))/2*A;

		bool retvalue = false;

		if((t0 > 0.1f) && (t0 < t)){
			t = t0;
			retvalue = true;
		}
		if((t1 > 0.1f) && (t1 < t)){
			t = t1;
			retvalue = true;
		}

		return retvalue;
	}

//*********************************************/
//			Ray-Triangle Intersection
//*********************************************/
	bool triangleIntersection(ray &r,SceneTriangle &tr,float &t){

		Vector e1 = tr.vertex[1] - tr.vertex[0];
		Vector e2 = tr.vertex[2] - tr.vertex[0];

		float epsilon = 0.00001;

		Vector p = r.dir.Cross(e2);
		float a = e1.Dot(p);

		// No Intersection Check 1
		if( a > -epsilon && a < epsilon)
			return false;

		float f = 1/a;
		Vector s = r.start - tr.vertex[0]; 
		float u = f*(s.Dot(p));
		
		// No Intersection Check 2
		if( u < 0.0 || u > 1.0)
			return false;

		Vector q = s.Cross(e1);
		float v = f * (r.dir.Dot(q));

		// No Intersection Check 3
		if(v < 0.0 || u+v > 1.0)
			return false;

		//Now accept
		t = f * (e2.Dot(q));
		if(t > 0.01f)
			return true;
		else{
			t = 0;
			return false;
		}
	}

//*********************************************************/
//		Generate Picture Through Perspective Projection
//*********************************************************/

	Vector generatePicturePerspective(float screenX, float screenY, float screenZ){
		float red = 0.0, green = 0.0, blue = 0.0;
		float coef = 1.0f;
		int level = 0;
				
		Vector pixelVector(screenX,screenY,screenZ);

		Vector rayStart(m_Scene.GetCamera().position.x,m_Scene.GetCamera().position.y,m_Scene.GetCamera().position.z);

		Vector dirVector = pixelVector - m_Scene.GetCamera().position;
		dirVector.Normalize();
		Vector rayDir = dirVector;

		ray viewRay = {rayStart, rayDir};
		int iteration = 0;
		float t = 0.0f;
		Vector color = trace(viewRay,iteration,t);
		
		return color;
	}

//*********************************************************/
//		Ray Tracer
//*********************************************************/

	Vector trace(ray viewRay,int iteration, float &t){
		Vector localColor(0.0f,0.0f,0.0f);
		Vector bgColor;
		Vector reflectColor;
		Vector refractColor;
		Vector matDiffIntrp, matSpecIntrp;
		Vector ambient(0.0f,0.0f,0.0f);

		t = 2000.0f;
		vector<float> tStore;
		vector<SceneObject *> objStore;
		vector<modelTrTracker> modelHitTr;
		vector<ray> refRayList;
		vector<ray> refractRayList;
		SceneObject *closestObj;
		SceneObject *obj;
		SceneMaterial mat;
		float red = 0.0f, green=0.0f, blue=0.0f;

		//for each object check intersection with viewRay
		for(int i=0;i < m_Scene.GetNumObjects();i++){ 
			obj = m_Scene.GetObject(i);
			if(obj->IsSphere()){
				if(sphereIntersection(viewRay, (*(static_cast<SceneSphere *>(obj))), t)){
					tStore.push_back(t);
					objStore.push_back(obj);
				}
			}
			if(obj->IsTriangle()){
				if(triangleIntersection(viewRay, (*(static_cast<SceneTriangle *>(obj))), t)){
					tStore.push_back(t);
					objStore.push_back(obj);
				}
			}

			modelTrTracker modelTr;

			if(obj->IsModel()){
				for(int i=0;i<(*(static_cast<SceneModel *>(obj))).GetNumTriangles();i++){
					if(triangleIntersection(viewRay, (*(*(static_cast<SceneModel *>(obj))).GetTriangle(i)), t)){
						tStore.push_back(t);
						objStore.push_back(obj);
						modelTr.index = i;
						modelTr.tValue = t;
						modelHitTr.push_back(modelTr);
					}
				}
			}

		}
		
		//if no intersection: Return BackGround Color
		if(tStore.empty()){
				return bgColor = m_Scene.GetBackground().color;
		}
			
		//get closest point of intersection and the object that that has the closest intersection
		float tMin = tStore[0];
		closestObj = objStore[0];
		for(int i=0;i<tStore.size();i++){
			if(tStore[i] < tMin){
				tMin = tStore[i];
				closestObj = objStore[i];
			}
		}

		t = tMin;

		//LightRay, ReflectionRay and RefractionRay will start from the new start point
		Vector newStartPoint(viewRay.start + viewRay.dir * (tMin));

		//Computing normal at intersection point and getting material properties
		Vector n(0.0,0.0,0.0);
		if(closestObj->IsSphere()){
			n = newStartPoint - (*(static_cast<SceneSphere *>(closestObj))).center;
			n.Normalize();
			mat = m_Scene.GetMaterial((*(static_cast<SceneSphere *>(closestObj))).material);
		}

		if(closestObj->IsTriangle()){
			SceneTriangle *tr = static_cast<SceneTriangle *>(closestObj);
			float trTotalArea = 0.5 * ((tr->vertex[1] - tr->vertex[0]).Cross(tr->vertex[2] - tr->vertex[0])).Magnitude();
			float trAreaA1 = 0.5 * ((tr->vertex[1] - newStartPoint).Cross(tr->vertex[2] - newStartPoint)).Magnitude(); 
			float trAreaA2 = 0.5 * ((tr->vertex[0] - newStartPoint).Cross(tr->vertex[2] - newStartPoint)).Magnitude(); 
			float trAreaA3 = 0.5 * ((tr->vertex[0] - newStartPoint).Cross(tr->vertex[1] - newStartPoint)).Magnitude(); 

			n = ((tr->normal[0] * trAreaA1) + (tr->normal[1] * trAreaA2) + (tr->normal[2] * trAreaA3))/trTotalArea;
			n.Normalize();

			matDiffIntrp = (m_Scene.GetMaterial(tr->material[0]).diffuse * trAreaA1 + 
								m_Scene.GetMaterial(tr->material[1]).diffuse * trAreaA2 +
								m_Scene.GetMaterial(tr->material[2]).diffuse * trAreaA3)/trTotalArea;

			matSpecIntrp = (m_Scene.GetMaterial(tr->material[0]).specular * trAreaA1 + 
								m_Scene.GetMaterial(tr->material[1]).specular * trAreaA2 +
								m_Scene.GetMaterial(tr->material[2]).specular * trAreaA3)/trTotalArea;

			mat = m_Scene.GetMaterial((*(static_cast<SceneTriangle *>(closestObj))).material[0]);
		}

		if(closestObj->IsModel()){
			SceneModel *model = static_cast<SceneModel *>(closestObj);
			int trIndex = 0;
			for(int i=0;i<modelHitTr.size();i++){
				if(tMin == modelHitTr[i].tValue){
					trIndex = modelHitTr[i].index;
					break;
				}
			}

			SceneTriangle *mtr = static_cast<SceneTriangle *>(model->GetTriangle(trIndex));
			float trTotalArea = 0.5 * ((mtr->vertex[1] - mtr->vertex[0]).Cross(mtr->vertex[2] - mtr->vertex[0])).Magnitude();
			float trAreaA1 = 0.5 * ((mtr->vertex[1] - newStartPoint).Cross(mtr->vertex[2] - newStartPoint)).Magnitude(); 
			float trAreaA2 = 0.5 * ((mtr->vertex[0] - newStartPoint).Cross(mtr->vertex[2] - newStartPoint)).Magnitude(); 
			float trAreaA3 = 0.5 * ((mtr->vertex[0] - newStartPoint).Cross(mtr->vertex[1] - newStartPoint)).Magnitude(); 

			n = ((mtr->normal[0] * trAreaA1) + (mtr->normal[1] * trAreaA2) + (mtr->normal[2] * trAreaA3))/trTotalArea;
			n.Normalize();

			matDiffIntrp = (m_Scene.GetMaterial(mtr->material[0]).diffuse * trAreaA1 + 
								m_Scene.GetMaterial(mtr->material[1]).diffuse * trAreaA2 +
								m_Scene.GetMaterial(mtr->material[2]).diffuse * trAreaA3)/trTotalArea;

			matSpecIntrp = (m_Scene.GetMaterial(mtr->material[0]).specular * trAreaA1 + 
								m_Scene.GetMaterial(mtr->material[1]).specular * trAreaA2 +
								m_Scene.GetMaterial(mtr->material[2]).specular * trAreaA3)/trTotalArea;

			mat = m_Scene.GetMaterial(mtr->material[0]);		
		}

		//Global Ambient Component
		ambient.x = m_Scene.GetBackground().ambientLight.x;
		ambient.y = m_Scene.GetBackground().ambientLight.y;
		ambient.z = m_Scene.GetBackground().ambientLight.z;

		//Computer Phong's Diffuse and Specular component for each light
		for(int i=0;i<m_Scene.GetNumLights();i++){
			SceneLight currentLight = m_Scene.GetLight(i);
			Vector lightDist = currentLight.position - newStartPoint;
			//dot product less than ZERO => in shadows
			if(n.Dot(lightDist) <= 0.0f)
				continue;
			float t = lightDist.Dot(lightDist);
			if(t <= 0.0f)
				continue;
			//LightRay construction
			ray lightRay;
			lightRay.start = newStartPoint;
			lightRay.dir = lightDist.Normalize();

			//Reflection Ray Construction
			reflectionRay.start = newStartPoint;
			reflectionRay.dir = (lightRay.dir - (n * (lightRay.dir.Dot(n))) * 2).Normalize();
			refRayList.push_back(reflectionRay);

			//Shadow computation
			bool inShadow = false;

			//for each object check if any object comes between light and the point being colored
			SceneObject *objInPathOfLight;
			for(int j=0;j<m_Scene.GetNumObjects();j++){
				objInPathOfLight = m_Scene.GetObject(j);
				if(objInPathOfLight->IsSphere()){
					if(sphereIntersection(lightRay, (*(static_cast<SceneSphere *>(objInPathOfLight))), t)){
						inShadow = true;
						break;
					}
				}
				if(objInPathOfLight->IsTriangle()){
					if(triangleIntersection(lightRay, (*(static_cast<SceneTriangle *>(objInPathOfLight))), t)){
						inShadow = true;
						break;
					}
				}

				if(objInPathOfLight->IsModel()){
					for(int i=0;i<(*(static_cast<SceneModel *>(obj))).GetNumTriangles();i++){
						if(triangleIntersection(lightRay, (*(*(static_cast<SceneModel *>(obj))).GetTriangle(i)), t)){
							inShadow = true;
							break;
						}
					}
				}
			}

			//Not in shadow: Calculate Phong Diffuse and Specular Terms
			if(!inShadow){
				//Diffuse
				float diff = lightRay.dir.Dot(n);
				//Specular
				Vector H = (lightRay.dir - viewRay.dir).Normalize();
				SceneBackground bg = m_Scene.GetBackground();
				//float spec = powf(H.Dot(n),mat.shininess);
				//float spec = powf(reflectionRay.dir.Dot(n),mat.shininess);
				float spec = powf(viewRay.dir.Dot(reflectionRay.dir),mat.shininess);

				if(diff > 0 || spec > 0){
					if(closestObj->IsTriangle() || closestObj->IsModel()){
						localColor.x += (diff * currentLight.color.x * matDiffIntrp.x) + spec * currentLight.color.x * matSpecIntrp.x;
						localColor.y += (diff * currentLight.color.y * matDiffIntrp.y) + spec * currentLight.color.y * matSpecIntrp.y;
						localColor.z += (diff * currentLight.color.z * matDiffIntrp.z) + spec * currentLight.color.z * matSpecIntrp.z;
					}else{
						localColor.x += (diff * currentLight.color.x * mat.diffuse.x) + spec * currentLight.color.x * mat.specular.x;
						localColor.y += (diff * currentLight.color.y * mat.diffuse.y) + spec * currentLight.color.y * mat.specular.y;
						localColor.z += (diff * currentLight.color.z * mat.diffuse.z) + spec * currentLight.color.z * mat.specular.z;
					}
				}
			}

		}		

		//Recursive Reflection
		Vector reflComp(0.0f,0.0f,0.0f);
		reflComp.x = mat.reflective.x;
		reflComp.y = mat.reflective.y;
		reflComp.z = mat.reflective.z;

		float EPSILON = 0.000001;
		if ((reflComp.x > 0.0f) && (iteration < 5))
		{
			Vector Ref = (viewRay.dir - (n * (viewRay.dir.Dot(n))) * 2.0f).Normalize();
			Vector rcol( 0.0f, 0.0f, 0.0f );

			ray refRay;
			refRay.start = newStartPoint + Ref * EPSILON;
			refRay.dir =Ref;
			rcol = trace(refRay, (iteration+1),t);

			//localColor.x = localColor.x + reflComp.x * rcol.x * mat.diffuse.x;
			//localColor.y = localColor.y + reflComp.y * rcol.y * mat.diffuse.y;
			//localColor.z = localColor.z + reflComp.z * rcol.z * mat.diffuse.z;

			localColor.x = (1 - reflComp.x) * localColor.x + reflComp.x * rcol.x;
			localColor.y = (1 - reflComp.y) * localColor.y + reflComp.y * rcol.y;
			localColor.z = (1 - reflComp.z) * localColor.z + reflComp.z * rcol.z;
		}

		//Recursive Refraction
		float refr = mat.refraction_index.x;

		if ((refr > 0) && (iteration < 5))
		{
			float rindex = refr;;
			float n1 = 1.0;
			float n12 = n1/rindex;
			Vector N(0.0f,0.0f,0.0f);

			if(closestObj->IsTriangle())
				N = n * (-1);
			else
				N = n;

			float c1 = (N.Dot(viewRay.dir));
			float c2 = 1.0f - n12 * n12 * (1.0f - c1 * c1);

			if(c2 > 0.0f){
				Vector T = ((viewRay.dir * n12) + (n * (n12 * c1 - sqrtf(c2)))).Normalize();
				Vector rfcol( 0.0f, 0.0f, 0.0f );

				ray refrRay;
				refrRay.start = newStartPoint + T * EPSILON;
				refrRay.dir = T;
				float dist;
				rfcol = trace(refrRay, (iteration+1),dist);

				//localColor.x = localColor.x + rfcol.x * mat.transparent.x;
				//localColor.y = localColor.y + rfcol.y * mat.transparent.y;
				//localColor.z = localColor.z + rfcol.z * mat.transparent.z;

				localColor.x = (1 - mat.transparent.x) * localColor.x + rfcol.x * mat.transparent.x;
				localColor.y = (1 - mat.transparent.y) * localColor.y + rfcol.y * mat.transparent.y;
				localColor.z = (1 - mat.transparent.x) * localColor.z + rfcol.z * mat.transparent.z;
			}

		}

		return Vector(min(localColor.x+ambient.x,1.0f),min(localColor.y+ambient.y,1.0f),min(localColor.z+ambient.z,1.0f));
	}

};

#endif // RAYTRACE_H
