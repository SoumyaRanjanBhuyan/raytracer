1. Menu Operation: Select a scene, then select how to render: Normal/Ray Tracer
2. While a second scene is ray traced, it may seem like no processing is going on. 
This is because of the same background color. 
Wait for a few seconds for the ray tracer to reach an object.

![Menu Options](https://bitbucket.org/SoumyaRanjanBhuyan/raytracer/raw/8be0d9de7a95570c294f3a0a22c60b6203bf490e/Screenshots/5.png)
![Reflective Cylinder](https://bitbucket.org/SoumyaRanjanBhuyan/raytracer/raw/1b2c524d08a3f744c447fb94cdc48e24a5cc0f68/Screenshots/3.png)
![Reflection](https://bitbucket.org/SoumyaRanjanBhuyan/raytracer/raw/1b2c524d08a3f744c447fb94cdc48e24a5cc0f68/Screenshots/2.png)
![Reflecttion + Refraction](https://bitbucket.org/SoumyaRanjanBhuyan/raytracer/raw/1b2c524d08a3f744c447fb94cdc48e24a5cc0f68/Screenshots/1.png)